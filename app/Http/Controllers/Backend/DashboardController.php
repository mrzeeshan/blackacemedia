<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\Visitor;
use App\Models\Department;
use App\Http\Requests;
use App\Http\Controllers\AppBaseController;

class DashboardController extends AppBaseController
{

    /**
     *
     * @param  App\Models\Visitor
     * @return void
     * 
     */
    function __construct(Visitor $visitors)
    {
        parent::__construct();

        $this->visitors = $visitors;
    }



    /**
     *
     * Dashboard Page
     * @return App\Models\Ticket
     */
    public function index()
    {   
        // Get only 7 tickets for the dashboard
        $tickets = Ticket::take(7)->orderBy('created_at', 'DESC')->get();

        // Ticket Counting
        $all_ticket_counting = Ticket::count();
        $new_ticket_counting = Ticket::where('status', 'new')->count();
        $pending_ticket_counting = Ticket::where('status', 'pending')->count();
        $solved_ticket_counting = Ticket::where('status', 'solved')->count();

        // Get clients
        $role = Role::where('name', 'client')->first();
        $clients = Role::find($role->id)->users()->orderBy('created_at', 'DESC')->take(6)->get();

        // Get staffs
        $role = Role::where('name', 'staff')->first();
        $staffs = Role::find($role->id)->users()->orderBy('created_at', 'DESC')->take(7)->get();

        $departments = Department::lists('name', 'id');

        // $visitors = $this->visitors->all();
        // $clients = $this->visitors->whereMonth('created_at', '=', date('m'))->where('type', 'client')->get();
        // $visitors = $this->visitors->whereMonth('created_at', '=', date('m'))->where('type', 'guest')->get();

        $data = compact( 'tickets', 'departments',  'all_ticket_counting', 'new_ticket_counting','pending_ticket_counting', 'solved_ticket_counting', 'clients', 'staffs');

        return view('dashboard.home', $data);
    }



    /**
     *
     * Return the authenticated user as JSON
     * @return App\Models\User
     */
    public function getAuthUser()
    {
        if( \Auth::check() ) {
            $user = \Auth::user();

            return \Response::json(['user' => $user]);
        }

    }
}
