<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Department;
use App\Models\Option;
use App\Models\Role;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends HomeBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Option $options)
    {
        parent::__construct();
        $this->options = $options;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // If application is not installed
        if (!file_exists(storage_path('installed'))) {
            return redirect('install');
        }

        // Get the theme style from database
        $style_option = $this->options
            ->where('name', 'style')
            ->first();

        $data = array();
        // Get all departments
        $data['departments'] = Department::all();
        if (isset($_GET['app-name']))
            $data['app-name'] = $_GET['app-name'];
        if (isset($_GET['app-version']))
            $data['app-version'] = $_GET['app-version'];
        if (isset($_GET['os-version']))
            $data['os-version'] = $_GET['os-version'];
        if (isset($_GET['phone-model']))
            $data['phone-model'] = $_GET['phone-model'];
        // Dynamically change view according to admin preference
        if ($style_option->value == 'homepage_style_1') {
            return view('home')->with('data', $data);
        } elseif ($style_option->value == 'homepage_style_2') {
            return view('homepage2')->with('data', $data);
        } else {
            return view('home')->with('data', $data);
        }
    }


    /**
     *
     * Homepage style 2
     * @return View
     *
     */
    public function homepage2()
    {
        if (isset($_GET['app-name']))
            $data['app-name'] = $_GET['app-name'];
        if (isset($_GET['app-version']))
            $data['app-version'] = $_GET['app-version'];
        if (isset($_GET['os-version']))
            $data['os-version'] = $_GET['os-version'];
        if (isset($_GET['phone-model']))
            $data['phone-model'] = $_GET['phone-model'];

        $data['departments'] = Department::all();
        return view('homepage2')->with('data', $data);
    }


    /**
     *
     * User Settings Page
     * @return View
     *
     */
    public function settings()
    {
        $user = \Auth::user();
        return view('profile.settings', compact('user'));
    }


}
