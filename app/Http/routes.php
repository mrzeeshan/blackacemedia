<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();


Route::get('/', 'HomeController@index');

Route::get('/homepage-2', 'HomeController@homepage2');


Route::post('create-tickets', [
	'as' => 'create.ticket',
	'uses' => 'TicketController@createTicket'
]);


// Ticket Route
Route::group(['middleware' => 'auth'], function() {

//	Route::post('create-tickets', [
//		'as' => 'create.ticket',
//		'uses' => 'TicketController@createTicket'
//	]);

	Route::get('edit-ticket/{id}', [
		'as' => 'edit.ticket', 
		'uses' => 'TicketController@getEditTickets'
	]);

	Route::post('edit-tickets/{id}', [
		'as' => 'edit.post.ticket', 
		'uses' => 'TicketController@postEditTickets'
	]);

	Route::get('all-tickets', [
		'as' => 'all.tickets', 
		'uses' => 'TicketController@allTickets'
	]);

	Route::get('ticket/{id}/{subject}', [
		'as' => 'single.ticket', 
		'uses' => 'TicketController@showTickets'
	]);

	// Ticket remove
	Route::post('/ticket/remove', [
		'as'	=> 	'dashboard.ticket.remove',
		'uses'	=> 	'Backend\TicketController@removeTicket',
	]);

});


// Reply Route
Route::group(['middleware' => 'auth'], function() {

	Route::post('{id}/create-reply', [
		'as' => 'create.reply', 
		'uses' => 'ReplyController@createReply'
	]);

	Route::post('/remove-reply/{id}', [
		'as' => 'remove.reply', 
		'uses' => 'ReplyController@removeReply'
	]);

});

// Profile Settings
Route::group(['middleware' => 'auth'], function() {

	Route::get('profile/settings', [
		'as' => 'proifle.settings', 
		'uses' => 'HomeController@settings'
	]);

	 // Update profile
	Route::post('/profile/update', [
		'as'	=> 	'profile.update',
		'uses'	=> 	'Backend\SettingsController@profileUpdate',
	]);


});

//////////////////////
// Dashboard Routes //
//////////////////////
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'role:admin|staff'], 'namespace' => 'Backend'], function() {

	Route::get('/', [
		'as' => 'dashboard.index', 
		'uses' => 'DashboardController@index'
	]);

	Route::post('/get-authenticated-user', [
		'as' => 'get.auth.user', 
		'uses' => 'DashboardController@getAuthUser'
	]);


	/////////////////////////////
	// Dashboard Ticket Routes //
	/////////////////////////////
	Route::get('/tickets/{view}', [
		'as'	=> 	'dashboard.tickets',
		'uses'	=> 	'TicketController@tickets',
	]);

	Route::get('/tickets', function() {
		return redirect('/dashboard/tickets/all');
	});

	Route::get('/tickets/{id}/{subject}', [
		'as' => 'dashboard.single.ticket', 
		'uses' => 'TicketController@showTickets'
	]);

	// assign ticket to staff
	Route::post('/assign/ticket/{id}', [
		'as'	=> 	'ticket.assign.user',
		'uses'	=> 	'TicketController@assignTicket',
	]);

	// update ticket status
	Route::post('/update/ticket/{id}/status', [
		'as'	=> 	'update.ticket.status',
		'uses'	=> 	'TicketController@updateStatus',
	]);



	// Ticket search
	Route::get('/ticket/search', [
		'as'	=> 	'dashboard.ticket.search',
		'uses'	=> 	'TicketController@searchTicket',
	]);


	///////////////////////////////////
	// Dashboard Noticiations Routes //
	///////////////////////////////////

	// Remove single notification

	Route::post('/remove/single/notification', [
		'as'	=> 	'remove.single.notification',
		'uses'	=> 	'NotificationController@removeNotif',
	]);

	// Remove all notification
	Route::post('/remove/all/notification', [
		'as'	=> 	'remove.all.notification',
		'uses'	=> 	'NotificationController@removeAllNotif',
	]);

	///////////////////////////////////////
	// Dashboard Staff and Clients route //
	///////////////////////////////////////


	// Dashbaord Staffs
	Route::get('/staffs/all', [
		'as'	=> 	'dashboard.staffs.all',
		'uses'	=> 	'StaffController@allStaffs',
	]);

	// Staff create
	Route::get('/staffs/create', [
		'as'	=> 	'dashboard.staffs.create',
		'uses'	=> 	'StaffController@getCreate',
	]);

	// Staff Create
	Route::post('/staffs/create', [
		'as'	=> 	'dashboard.staffs.postCreate',
		'uses'	=> 	'StaffController@create',
	]);

	// Get staff edit ajax result
	Route::post('/staffs/edit', [
		'as'	=> 	'dashboard.staffs.getEdit',
		'uses'	=> 	'StaffController@getEdit',
	]);

	// Staff Update
	Route::post('/staffs/update', [
		'as'	=> 	'dashboard.staffs.update',
		'uses'	=> 	'StaffController@update',
	]);

	// Get Staff edit page
	Route::get('/staffs/get-edit/{id}', [
		'as'	=> 	'dashboard.staffs.getPageEdit',
		'uses'	=> 	'StaffController@getPageEdit',
	]);


	// Staff Remove
	Route::post('/staffs/remove', [
		'as'	=> 	'dashboard.staffs.remove',
		'uses'	=> 	'StaffController@removeStaff',
	]);

	// Dashbaord Clients
	Route::get('/clients/all', [
		'as'	=> 	'dashboard.clients.all',
		'uses'	=> 	'ClientController@allClients',
	])->middleware('staffPermission');

	Route::post('/clients/create', [
		'as'	=> 	'dashboard.clients.create',
		'uses'	=> 	'ClientController@create',
	]);

	Route::post('/clients/remove', [
		'as'	=> 	'dashboard.clients.remove',
		'uses'	=> 	'ClientController@removeClient',
	]);



	///////////////////////////////////
	// Dashboard Settings Page Route //
	///////////////////////////////////

	// Settings page
	Route::get('/settings', [
		'as'	=> 	'dashboard.settings.index',
		'uses'	=> 	'SettingsController@index',
	]);

	// General Site Settings
	Route::post('/general/settings', [
		'as'	=> 	'dashboard.general.settings',
		'uses'	=> 	'SettingsController@generalSettings',
	]);

	// Theme Update Settings
	Route::post('/theme/update', [
		'as'	=> 	'dashboard.theme.update',
		'uses'	=> 	'SettingsController@themeUpdate',
	]);

	// Add  Department
	Route::post('/add/department', [
		'as'	=> 	'dashboard.add.department',
		'uses'	=> 	'SettingsController@addDepartment',
	]);

	// Edit  Department
	Route::post('/edit/department/{id}', [
		'as'	=> 	'dashboard.edit.department',
		'uses'	=> 	'SettingsController@editDepartment',
	]);

	// Remove  Department
	Route::post('/remove/department/{id}', [
		'as'	=> 	'dashboard.remove.department',
		'uses'	=> 	'SettingsController@removeDepartment',
	]);

	// Theme Update Settings
	Route::post('/app/settings', [
		'as'	=> 	'dashboard.app.settings',
		'uses'	=> 	'SettingsController@appSettings',
	]);




	///////////////////////
	// APPLICATIO UPDATE //
	///////////////////////

	Route::get('/update', [
		'as'	=> 	'dashboard.update',
		'uses'	=> 	'UpdateController@index',
	])->middleware('role:admin');


});
