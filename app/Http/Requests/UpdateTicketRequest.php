<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateTicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'app_name' => 'required|min:3',
            'app_version' => 'required|min:1',
            'os_version' => 'required|min:1',
            'phone_model' => 'required|min:3',
            'department_id' => 'required',
            'message' => 'required|min:15',
            'file' => 'mimes:jpg,jpeg,bmp,png,doc,docx,xls,csv'
        ];
    }
}
