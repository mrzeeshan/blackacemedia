$(document).ready(function() {


    /*=======================================================
    =             Ajax Ticket Submission            =
    =======================================================*/    

    // On  focusing the form elements
    $('.subject').on('focus', function() {
        // Check the user if logged in or not
        if( loginStatus !== 'logged' )  {
            loginAlert();
        }

    });


    // On clicking the submit button 
    $('.create-ticket-btn').on('click', function(e) {
        e.preventDefault();

        // Check the user if logged in or not
        if( loginStatus !== 'logged' )  {
            // If not logged in, then alert them.
            loginAlert();
        } else {

            // Initialize variables
            var route= $('#createTicket').attr('action'),
                data = new FormData( $("#createTicket")[0] ),
                name = $('.name').val(),

                email = $('.email').val(),
                app_name = $('.app_name').val(),
                app_version = $('.app_version').val(),
                os_version = $('.os_version').val(),
                phone_model = $('.phone_model').val(),
                department = $('.department').val(),
                message = $('.message').val(),
                valid = '';




            if(email.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.email-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Email </strong> is required');
            } else {
                valid = '';
                $('.email-group').removeClass('has-error').find('.help-block').html(' ');
            }

            if(app_name.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.app-name-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> App Name </strong> is required');
            } else {
                valid = '';
                $('.app-name-group').removeClass('has-error').find('.help-block').html(' ');
            }




            if(phone_model.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.phone-model-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Phone Model </strong> is required');
            } else {
                valid = '';
                $('.phone-model-group').removeClass('has-error').find('.help-block').html(' ');
            }



            if(app_version.length <1) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.app-version-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> App Version </strong> is required');
            } else {
                valid = '';
                $('.app-version-group').removeClass('has-error').find('.help-block').html(' ');
            }


            if(os_version.length <1) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.os-version-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> OS Version </strong> is required');
            } else {
                valid = '';
                $('.os-version-group').removeClass('has-error').find('.help-block').html(' ');
            }




            if(email.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.email-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Email </strong> is required');
            } else {
                valid = '';
                $('.email-group').removeClass('has-error').find('.help-block').html(' ');
            }



            if(email.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.email-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Email </strong> is required');
            } else {
                valid = '';
                $('.email-group').removeClass('has-error').find('.help-block').html(' ');
            }



            if(email.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.email-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Email </strong> is required');
            } else {
                valid = '';
                $('.email-group').removeClass('has-error').find('.help-block').html(' ');
            }



            // Check if the message length is less than or equal to 10
            if( ! message) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.message-group').removeClass('has-success').addClass('has-error').find('.help-block').html('<strong> Message </strong> is required');
            } else {
                valid = '';
                $('.message-group').removeClass('has-error').find('.help-block').html(' ');
            }

            // Check if the department is selected
            if( ! department ) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.department-group').removeClass('has-success').addClass('has-error').find('.help-block').html('<strong> Support Department </strong> is required');
            } else {
                valid = '';
                $('.department-group').removeClass('has-error').find('.help-block').html(' ');
            }

            // Check if the subject length is less than or equal to 3
            if(name.length <= 3) {
                valid = 'invalid';
                $('.form-status').removeClass('alert').html(' ');
                $('.name-group').removeClass('has-success').addClass('has-error').find('.help-block').html(' <strong> Name </strong> is required');
            } else {
                valid = '';
                $('.name-group').removeClass('has-error').find('.help-block').html(' ');
            }

            if( valid == '') {
                captcha = grecaptcha.getResponse();
                if( captcha.length == 0 ) {
                    $('.form-status').removeClass('alert-warning').addClass('animated fadeInUp alert alert-warning').html(' Please complete the recaptcha. ');
                } else {
                    $('.form-status').removeClass('alert-danger').html(' ');

                    // Perform the AJAX Ticket Creating Submission
                    ajaxTicketCreate(route, data);
                }

            }
        }


        /*----------  Ajax Ticket Creation  ----------*/
        
        function ajaxTicketCreate(route, data) {
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.form-status').removeClass('alert-warning').addClass('animated fadeInUp alert alert-info').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing data...');
                },
                success: function(data) {
                     $('.form-status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Your ticket has been submitted.');

                     // Reset the form
                     $("#createTicket")[0].reset();
                     $('.ticket-submit-file label span').html('<span class="browse">Browse</span> your file here…');
                     grecaptcha.reset();
                },
                error: function(data) {

                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.form-status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });
        }
    });
    
    
    
    
    /*=====  Login Alert  ======*/
    
    function loginAlert() {
        //swal({
        //    title: "Login Required",
        //    type: 'error',
        //    text: "Please <a href='/login'>login</a> first to submit your ticket.",
        //    html: true,
        //});
    }


});















