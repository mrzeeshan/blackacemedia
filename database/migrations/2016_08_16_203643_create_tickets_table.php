<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('department_id');
            $table->string('subject');
            $table->string('email');
            $table->string('app_name');
            $table->string('app_version');
            $table->string('os_version');
            $table->string('phone_model');
            $table->text('message');
            $table->string('files')->nullable();
            $table->enum('status', ['new', 'pending', 'solved']);
            $table->integer('assigned_to')->nullable();
            $table->integer('solved_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
