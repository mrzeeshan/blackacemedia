
                        <!--====  TICKET SETTINGS  ====-->

                        <div role="tabpanel" class="tab-pane" id="ticket_s">

                            {{ Form::open([
                                'route' => 'dashboard.add.department',
                                'method' => 'post',
                                'id' => 'add_department_form'
                            ])}}

                                <div class="form-group clearfix deparment_form_group">
                                    <div class="form-input ">
                                        {{ Form::label('name', 'Add new department')}}
                                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Write department name']) }}
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-required-lable department-fg">
                                        <button class="btn btn-success add_department_btn" type="submit">Add</button>
                                    </div>
                                </div>
                                <!-- form-group ends -->

                            {{ Form::close() }}

                            <div class="form-group clearfix">
                                <label for="extra_css">Current departments</label>
                                <div class="all-departments">
                                    <ul>
                                        @if( count( $departments ) )
                                            @foreach( $departments as $department )
                                                <li class="list-department">
                                                    <span class="content">
                                                        <span class="name">{{ $department->name }}</span>
                                                        <input type="text" name="name" id="input_name" onBlur="editDepartmentFunc(this, {{ $department->id }});" value="{{ $department->name }}">
                                                    </span>
                                                    <span class="edit">
                                                        <button value="{{ $department->id }}" ><i class="fa fa-edit"></i></button>
                                                    </span>
                                                    <span class="delete">
                                                        <button value="{{ $department->id }}" ><i class="fa fa-times"></i></button>
                                                    </span>
                                                </li>
                                                <!-- Ends list-department -->
                                            @endforeach
                                        @else
                                            <li class="list-department">
                                                Nothing found
                                            </li>

                                        @endif
                                    </ul>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-status"></div>

                            </div>
                        </div>