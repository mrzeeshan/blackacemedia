<?php
$departments = $data['departments'];
?>

<div class="ticket-form">
    {{ Form::open([
        'route' => 'create.ticket', 
        'method' => 'post', 
        'id' => 'createTicket', 
        'enctype' => 'multipart/form-data'
    ])}}

    <div class="form-group name-group">

        {{ Form::text('name', null, ['class' => 'form-control name', 'placeholder' => trans('messages.homepage.placeholders.name') ]) }}
        <span class="help-block "></span>

    </div>


    <div class="form-group email-group">
        {{ Form::email('email', null, ['class' => 'form-control email', 'placeholder' => trans('messages.homepage.placeholders.email') ]) }}
        <span class="help-block "></span>

    </div>

    <div class="form-group app-name-group">

        {{ Form::text('app_name', isset($data['app-name'])?$data['app-name']:null, ['class' => 'form-control app_name', 'placeholder' => trans('messages.homepage.placeholders.app_name') ]) }}
        <span class="help-block "></span>

    </div>

    <div class="form-group app-version-group">

        {{ Form::text('app_version', isset($data['app-version'])?$data['app-version']:null, ['class' => 'form-control app_version', 'placeholder' => trans('messages.homepage.placeholders.app_version') ]) }}
        <span class="help-block "></span>

    </div>


    <div class="form-group os-version-group">

        {{ Form::text('os_version', isset($data['os-version'])?$data['os-version']:null, ['class' => 'form-control os_version', 'placeholder' => trans('messages.homepage.placeholders.os_version') ]) }}
        <span class="help-block "></span>

    </div>


    <div class="form-group phone-model-group">
        {{ Form::text('phone_model', isset($data['phone-model'])?$data['phone-model']:null, ['class' => 'form-control phone_model', 'placeholder' => trans('messages.homepage.placeholders.phone_model') ]) }}
        <span class="help-block "></span>
    </div>


    <div class="form-group department-group">

        <select name="department_id" class="form-control department">
            <option disabled selected value> {{ trans('messages.homepage.placeholders.department') }} </option>
            @foreach($departments as $department)
                <option value="{{ $department->id }}"> {{ $department->name }} </option>
            @endforeach
        </select>
        <span class="help-block"></span>

    </div>


    <div class="form-group message-group">

        {{ Form::textarea('message', null, ['class' => 'form-control message', 'placeholder' =>trans('messages.homepage.placeholders.message'), 'wrap' => 'hard', 'onkeyup' => 'autoGrow(this);' ]) }}
        <span class="help-block"></span>

    </div>


