@extends('layouts.auth')

@section('title', trans('auth.reset_password_title') . ' | ' . Options::get('title') )

<!-- Main Content -->
@section('content')
<p class="title"> {{ trans('auth.reset_password_title') }} </p>

<div class="the-form">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group">
                              <span class="input-group-addon" id="email_addon">
                                  <i class="fa fa-user"></i>
                              </span>
                                {{ Form::email('email', null, ['placeholder' => trans('auth.placeholders.email'), 'class' => 'form-control', 'aria-describedby' => 'email_addon']) }}
                            </div>
                            @if( $errors->any() ) 
                                <span class="help-block"> {{ $errors->first('email') }} </span>
                            @endif
                        </div>

                        <input class="btn btn-success" type="submit" value=" {{ trans('auth.reset_password_title') }} ">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
