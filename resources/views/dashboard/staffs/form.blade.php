@extends('layouts.dashboard')

@section('title', $staff->exists ? trans('messages.addNewStaff.editTitle') . ' | ' : trans('messages.addNewStaff.title') . ' | ' . Options::get('title') )

@section('content')

                <div class="content">
                    <div class="row content-body">
						<div class="get-create-form settings-tab">
						    <h4>{{ $staff->exists ? 
						    trans('messages.addNewStaff.editTitle') : 
						    trans('messages.addNewStaff.title') }}</h4>

						    {{ Form::model($staff, [
						    	'route' => $staff->exists ? 'dashboard.staffs.update' : 'dashboard.staffs.create', 
						    	'method' => 'post', 
						    	'id' => 'add_staffs_form'
						    ]) }}

						    	@if( $staff->exists )
									<input type="hidden" name="staff_id" value="{{ $staff->id }}">
						    	@endif

						        <div class="form-group staff_first_name_group">
						            {{ Form::label( trans('messages.addNewStaff.firstName') ) }}
						            {{ Form::text('first_name', null, ['class' => 'form-control staff_first_name', 'placeholder' => trans('messages.addNewStaff.placeholders.first_name') ])}}
						        </div>

						        <div class="form-group staff_email_group">
						            {{ Form::label( trans('messages.addNewStaff.email')  ) }}
						            {{ Form::email('email', null, ['class' => 'form-control staff_email', 'placeholder' => trans('messages.addNewStaff.placeholders.email') ])}}
						        </div>


						        <div class="form-group staff_password_group">
						            {{ Form::label( trans('messages.addNewStaff.password') ) }}
						            {{ Form::password('password', ['class' => 'form-control staff_password'])}}
						        </div>

						        <div class="form-group staff_img_g">
						            <label for="staff_img"> {{trans('messages.addNewStaff.staffImage') }} </label>
						            <input type="file" name="staff_img" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} files selected" multiple />
						            <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span> {{trans('messages.addNewStaff.file') }} </span></label>
						        </div>

						        <div class="form-group staff_department_group">
						            {{ Form::label('department_id', trans('messages.addNewStaff.department') ) }}
						            {{ Form::select('department_id', $departments, null, ['class' => 'form-control staff_department'])}}
						        </div>

						        <div class="form-group role_group">
						            {{ Form::label( trans('messages.addNewStaff.roleName') ) }}
						            {{ Form::text('role_name', null, ['class' => 'form-control staff_role', 'placeholder' => trans('messages.addNewStaff.placeholders.role_name') ])}}
						        </div>

						        <div class="form-status staff_form_status"></div>


						        <div class="form-group  submit-btns">
						            <button type="submit" class="btn btn-success">{{$staff->exists ? 
						            trans('messages.addNewStaff.updateStaffBtn')  : 
						            trans('messages.addNewStaff.addStaffBtn') }}</button>
						        </div>
						    {{ Form::close() }}
						</div>
                    </div>
                </div>

@stop
