@extends('layouts.dashboard')

@section('title',   trans('messages.settings.pageTitle')  . Options::get('title') )

@section('content')


<div class="content">

    <div class="row content-body ticket-single-page">
        <div class="col-lg-9 col-md-12 ticket-details">
            <div class="ticket-box">
                <div class="ticket-header">
                    <h4>{{ trans('messages.settings.title') }}</h4>
                </div>
                <div class="settings-tab">
                <ul class="nav nav-tabs" role="tablist">

                        @if( Auth::user()->hasRole('admin') )
                            <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">{{ trans('messages.settings.generalSettings') }}</a></li>
                            <li role="presentation"><a href="#theme" aria-controls="theme" role="tab" data-toggle="tab">{{ trans('messages.settings.themeSettings') }}</a></li>
                            <li role="presentation"><a href="#ticket_s" aria-controls="ticket_s" role="tab" data-toggle="tab">{{ trans('messages.settings.tiketSettings') }}</a></li>
                            <li role="presentation"><a href="#app_settings" aria-controls="app_settings" role="tab" data-toggle="tab">{{ trans('messages.settings.appSettings') }}</a></li>
                        @endif
                        <li role="presentation" class="{{ Auth::user()->hasRole('staff') ? 'fullwidth-li active' : '' }}"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('messages.settings.yourProfile') }}</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        @if( Auth::user()->hasRole('admin') )

                            {{-- Including GENERAL SETTINGS Partials --}}
                           @include('partials.dashboard.settings.general')


                            {{-- Including THEME SETTINGS Partials --}}
                           @include('partials.dashboard.settings.theme')


                            {{-- Including TICKET SETTINGS Partials --}}
                            @include('partials.dashboard.settings.ticket')

                            {{-- Including APP SETTINGS Partials --}}
                            @include('partials.dashboard.settings.app_settings')

                        @endif
                        

                        {{-- Including PROFILE SETTINGS Partials --}}
                        @include('partials.dashboard.settings.profile')

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


@stop
