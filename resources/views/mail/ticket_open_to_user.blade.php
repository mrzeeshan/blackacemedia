Hello {{ $user->fullName() }},
<br>

We have received your ticket. You will get a reply from us as soon as possible.
<br>
View ticket -> <a href="{!! route('single.ticket', [ 'id' => $ticket->id, 'subject' => $ticket->FormatSubject() ]) !!}"> {{ $ticket->subject }} </a> 
<br>
