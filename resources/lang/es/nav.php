<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |
    */

    'home' => 'Inicio',
    'about' => 'Acerca',
    'contact' => 'Contacto',
    'dashboard' => 'Tablero',
    'settings' => 'Ajustes',
    'all_tickets' => 'Tickets',
    'login' => 'Iniciar',
    'register' => 'Registro',
    'logout' => 'Salir',
    'go_to_admin' => 'Panel Admin',

];
