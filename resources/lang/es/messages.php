<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Instalador Ticket',
    'next' => 'Siguiente',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Bienvenido',
        'message' => 'Bienvenido al instalador de Tickets Dicex',
    ],


    /**
     *
     * Home page view translations.
     *
     */
    'homepage' => [
        'facing_problem'   => 'Buscar problema relacionado...',
        'form_title' => 'Necesita ayuda?  <span>Envie un ticket!</span>',
        'form_subtitle' => 'Responderemos su ticket a la brevedad.',

        'placeholders' => [
            'subject'   => 'Asunto',
            'name' => 'Name',
            'email' => 'Email Address',
            'app_name' => 'App Name',
            'app_version' => 'App Version',
            'os_version' => 'OS Version',
            'phone_model' => 'Phone Model',
            'department'   => '¿A que departamento pertenece?',
            'message'   => 'Escribe tu mensaje aqui',
            'file'   => '<span class="browse">Buscar</span> archivo&hellip;',
            'submit_ticket' => 'Enviar Ticket',

        ]
    ],



    /**
     *
     * Dashboard Language translations
     *
     */
    'dashboard' => [
        'pageTitle' => 'Tablero | ',
        'allTickets' => 'Todos los Tickets',
        'newTickets' => 'Tickets Nuevos',
        'solvedTickets' => 'Tickets Resueltos',
        'pendingTickets' => 'Tickets Pendientes',
        'recentTickets' => 'Tickets Recientes',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Titulo Ticket',
        'department' => 'Departamento',
        'date' => 'Fecha',
        'client' => 'Empleado',
        'status' => 'Estatus',
        'noTickets' => 'Ningun ticket encontrado',
        'newClient' => 'Nuevos Empleados',
        'allClient' => 'Todos los Empleados',
        'noClients' => 'Ningun empleado',
        'staffStatus' => 'Personal Estatus',
        'allStaffs' => 'Todo el Personal',
        'staffName' => 'Nombre Personal',
        'assignedTask' => 'Ticket Asignado',
        'solved' => 'Resueltos',
        'noStaffs' => 'Ningun ticket asignado',
        'statisticsTitle' => 'Estadisticas de este mes',
    ],
    


    /**
     *
     * Tickets page language Translations
     *
     */
    
    'tickets'   => [
        'pageTitle' => 'Buscar por ',
        'title' => ' Tickets | ',
        'tickets' => ' Tickets',
        'searchTitle' => 'Mostrando resultados de',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Ticket titulo',
        'department' => 'Departamento',
        'date' => 'Fecha',
        'client' => 'Empleado',
        'status' => 'Estatus',
        'action' => 'Accion',
        'noTickets' => 'Ningun ticket encontrado',
    ],



    /**
     *
     * Single Ticket page language Translations
     *
     */
    'singleTicket' => [
        'id' => '#ID:',
        'attachedFile' => 'Archivo Adjunto:',
        'ticketID' => 'Ticket ID:',
        'client' => 'Empleado',
        'supportType' => 'Departamento:',
        'ticketStatus' => 'Ticket Estatus:',
        'assignedTicket' => 'Ticket Asignado:',
        'solvedBy' => 'Resuelto por:',
        'assignNewTicket' => 'Asignar Ticket',
        'assignThisTicket' => 'Asignar Este Ticket',
        'notAssignedYet' => 'Sin asignar aun.',
        'noStaffsFound' => 'Ningun personal encontrado',
        'updateStatus' => 'Actualizar Estatus',
        'updateTicketStatus' => 'Actualizar Estatus del Ticket',
        'solved' => 'Resuelto',
        'pending' => 'Pendiente',
    ],


    /**
     *
     * Replies language translations
     *
     */
    'replies' => [
        'label' => 'Responder este ticket',
        'AttachedFIle' => 'Archivo Adjunto:',
        'attachFIle' => 'Adjuntar Archivo&hellip;',
        'sendReply' => 'Enviar',
    ],


    /**
     *
     * All Clients
     *
     */
    'allClients'    => [
        'pageTitle' => 'Empleados | ',
        'title' => 'Empleados',
        'name' => 'Nombre',
        'email' => 'E-mail',
        'clientImage' => 'Imagen',
        'registeredAt' => 'Registrado',
        'action' => 'Accion',
        'noClients' => 'Ningun empleado encontrado.',
    ],


    /**
     *
     * Dashboard Add new clients language translations
     *
     */
    'addNewClient'  => [
        'title'  => 'Añadir nuevo Empleado',
        'editTitle'  => 'Editar Empleado',
        'first_name'  => 'Nombre',
        'last_name'  => 'Apellidos',
        'email'  => 'E-mail',
        'password'  => 'Contraseña',
        'clientImage'  => 'Imagen Empleado',
        'addClientBtn'  => 'Agregar Empleado',
        'editClientBtn'  => 'Edit Client',
        'placeholders' => [
            'firstName'   => 'Ejemplo',
            'lastName'   => 'Ejemplo',
            'email'   => 'ejemplo@dicex.com',
            'file'   => 'Elegir imagen&hellip;',
        ],
    ],


    /**
     *
     * Dashboard Add new Staff language translations
     *
     */
    'addNewStaff'  => [
        'title'  => 'Añadir nuevo personal',
        'editTitle'  => 'Editar personal',
        'firstName'  => 'Nombre',
        'email'  => 'E-mail',
        'password'  => 'Contraseña',
        'department'  => 'Departamento',
        'roleName'  => 'Puesto',
        'permission'  => 'permission',
        'staffImage'  => 'Imagen Personal',
        'addStaffBtn'  => 'Agregar Personal',
        'updateStaffBtn'  => 'Actualizar',
        'placeholders' => [
            'first_name'   => 'Ejemplo',
            'lastName'   => 'Ejemplo',
            'email'   => 'ejemplo@dicex.com',
            'role_name'   => 'Soporte Tecnico',
            'perm_name'   => 'Permisos Desarrollador',
            'file'   => 'Elegir imagen&hellip;',
        ],
    ],



    /**
     *
     * All Staffs language translations
     *
     */
    'allStaffs' => [
        'pageTitle' => 'Todo el Personal | ',
        'title' => 'Personal',
        'name' => 'Nombre',
        'email' => 'E-mail',
        'department' => 'Departamento',
        'staffImage' => 'Imagen Personal',
        'assignedTickets' => 'Tickets Asignados',
        'solvedTickets' => 'Tickets Resueltos',
        'registeredAt' => 'Registrado',
        'action' => 'Accion',
        'noStaffs' => ' Ningun personal que mostrar.',
    ],


    /**
     *
     * Settings page Languages translations
     *
     */
    'settings' => [
        'pageTitle' => 'Ajustes | ',
        'title' => 'Ajustes',
        'generalSettings' => 'Generales',
        'themeSettings' => 'Apariencia',
        'tiketSettings' => 'Tickets',
        'appSettings' => 'E-mail',
        'yourProfile' => 'Perfil',
    ],



    /**
     *
     * Sidebar view page translations
     *
     */
    'sidebar'   => [
        'mainMenu'  => 'Menu Principal',
        'dashboard'  => 'Tablero',
        'tickets'  => 'Tickets',
        'allTickets'  => 'Todos los tickets',
        'newTickets'  => 'Tickets Nuevos',
        'pendingTickets'  => 'Tickets Pendientes',
        'solvedTickets'  => 'Tickets Resueltos',
        'staffs' => 'Personal',
        'allStaffs' => 'Todo el personal',
        'newStaffs' => 'Nuevo personal',
        'clients' => 'Empleados',
        'allclients' => 'Todos los empleados',
        'newclients' => 'Nuevo Empleado',
        'settings' => 'Ajustes',
        'logout' => 'Salir',
        'home' => 'Inicio',
        'notifications' => 'Notificaciones',
        'noNotifications' => 'Sin notificaciones',
        'clearAll' => 'Limpiar',
    ],



    /**
     *
     * Nav page translations
     *
     */
    'nav' => [
        'settings' => 'Ajustes',
        'logout' => 'Salir',
        'notifications' => 'Notifications',
        'noNotifications' => 'No Notifications to show',
        'clearAll' => 'Borrar todos',
        'placeholders' => [
            'formSearch' => 'Buscar ticket por ID o nombre'
        ],
    ],
    



    /**
     *
     * All tickets page frontend
     *
     */
    'all_Tickets' => [
        'pageTitle' => 'Todos los Tickets | ',
        'title' => 'Todos los Tickets',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Ticket titulo',
        'department' => 'Departamento',
        'date' => 'Fecha',
        'status' => 'Estado',
        'action' => 'Accion',
        'noTickets' => 'Ningun ticket.',
    ],



    /**
     *
     * Single ticket page frontend
     *
     */
    'allTickets' => [
        'id' => '#ID: ',
        'attachedFile' => '#ID: ',
    ],

    'errors' => [
        'siteRegistrations'  => 'Disculpe, actualmente el sitio no permite registros. ',
    ],



    /**
     *
     * Edit tickets page frontend
     *
     */
    'editTickets' => [
        'pageTitle' => 'Editar Tickets -  ',
        'attachFIle' => 'Archivo adjunto:',
        'file' => '<span class="browse">Buscar</span> su archivo here&hellip;',
        'fileAttachWarning' => 'Al añadir nuevo archivo, antiguo archivo sera <strong>borrado.</strong> No puedes añadir mas de 1 archivo a la vez.',
        'updateBtn' => 'Actualizar ticket',
        'placeholders' => [
            'subject' => 'Asunto',
            'message' => 'Escriba su mensaje aqui',
        ],
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requerimientos',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permisos',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Ajustes de Desarrollo',
        'save' => 'Guardar .env',
        'success' => 'Su archivo de configuracion .env ah sido guardado.',
        'errors' => 'Imposible guardar archivo .env, Favor de crearlo manualmente.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Listo',
        'finished' => 'Aplicacion instalada correctamente.',
        'exit' => 'Salir',
    ],
];
