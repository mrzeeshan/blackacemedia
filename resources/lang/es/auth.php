<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'No existen registros con estos datos.',
    'throttle' => 'Demasiados intentos. Porfavor intenta en :seconds segundos.',
    'password_reset_message' => 'Restaurar Contraseña',
    'loginTitle' => 'Iniciar Sesion',
    'registerTitle' => 'Registro',
    'reset_password_title' => 'Restablecer contraseña',
    'remember' => 'Recordarme?',
    'login_btn' => 'Iniciar',
    'register_btn' => 'Registrar',
    'register_now' => 'Registro',
    'forget_password' => '¿Olvidaste Contraseña?',
    'already_user' => '¿Ya eres usuario? Inicia sesion',

    'placeholders' => [
        'first_name'    => 'Nombre',
        'last_name'    => 'Apellido',
        'email'    => 'Email',
        'password'    => 'Contraseña',
        'confirm_password'    => 'Confirmar Contraseña',
    ]

];
