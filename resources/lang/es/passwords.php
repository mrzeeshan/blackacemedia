<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe ser al menos seis caracteres y ser iguales.',
    'reset' => 'Tu contraseña ah sido restaurada!',
    'sent' => 'Hemos enviado un e-mail con un link para restaurar tu contraseña!',
    'token' => 'Este token de contraseña es invalido.',
    'user' => "No podemos encontrar el usuario con ese e-mail.",

];
