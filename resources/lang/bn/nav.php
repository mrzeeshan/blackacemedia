<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |
    */

    'home' => 'বাড়ি',
    'about' => 'About',
    'contact' => 'Contact',
    'dashboard' => 'Dashboard',
    'settings' => 'Settings',
    'all_tickets' => 'All tickets',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'go_to_admin' => 'Go to Admin Panel',

];
