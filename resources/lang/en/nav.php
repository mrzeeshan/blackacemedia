<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |
    */

    'home' => 'Home',
    'about' => 'About',
    'contact' => 'Contact',
    'dashboard' => 'Dashboard',
    'settings' => 'Settings',
    'all_tickets' => 'All tickets',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'go_to_admin' => 'Go to Admin Panel',

];
